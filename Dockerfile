ARG CRATE_NAME=silevril

FROM rustembedded/cross:x86_64-pc-windows-gnu-0.2.1 as build-windows
ARG CRATE_NAME
ARG RUST_TARGET=x86_64-pc-windows-gnu
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup target add $RUST_TARGET

WORKDIR /$CRATE_NAME
RUN USER=root cargo init --bin --name $CRATE_NAME

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --target $RUST_TARGET --release

RUN rm -f target/${RUST_TARGET}/release/deps/silevri*
RUN rm src/*.rs
COPY . .
RUN cargo build --target $RUST_TARGET --release
RUN echo "successfully built an ${RUST_TARGET} executable"

FROM rustembedded/cross:x86_64-unknown-linux-musl as build-linux
ARG CRATE_NAME
ARG RUST_TARGET=x86_64-unknown-linux-musl
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup target add $RUST_TARGET

WORKDIR /$CRATE_NAME
RUN USER=root cargo init --bin --name $CRATE_NAME

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --target $RUST_TARGET --release

RUN rm -f target/${RUST_TARGET}/release/deps/silevri*
RUN rm src/*.rs
COPY . .
RUN cargo build --target $RUST_TARGET --release
RUN echo "successfully built an ${RUST_TARGET} executable"

# FROM ubuntu:20.04 as build-macos
# ARG CRATE_NAME
# ARG RUST_TARGET=x86_64-apple-darwin
# RUN apt-get update && \
#     DEBIAN_FRONTEND=noninteractive apt-get upgrade -yy && \
#     DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yy \
#     build-essential \
#     tzdata \
#     ca-certificates \
#     clang \
#     cmake \
#     curl \
#     git \
#     libgmp-dev \
#     libmpc-dev \
#     libmpfr-dev \
#     libssl-dev \
#     libxml2-dev \
#     llvm \
#     patch \
#     wget \
#     xz-utils \
#     zlib1g-dev

# RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
# ENV PATH="/root/.cargo/bin:${PATH}"
# RUN rustup target add $RUST_TARGET

# RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yy \
#     cmake \
#     llvm

# RUN git clone https://github.com/tpoechtrager/osxcross /opt/osxcross
# WORKDIR /opt/osxcross
# RUN curl -L https://s3.dockerproject.org/darwin/v2/MacOSX10.10.sdk.tar.xz -o tarballs/MacOSX10.10.sdk.tar.xz
# RUN UNATTENDED=yes OSX_VERSION_MIN=10.7 ./build.sh
# RUN ln -s /opt/osxcross/target/bin/* /usr/local/bin/

# WORKDIR /$CRATE_NAME
# RUN USER=root cargo init --bin --name $CRATE_NAME

# ENV LIBZ_SYS_STATIC=1 \
#     CARGO_TARGET_X86_64_APPLE_DARWIN_LINKER=x86_64-apple-darwin14-clang \
#     CARGO_TARGET_X86_64_APPLE_DARWIN_AR=x86_64-apple-darwin14-ar \
#     CC_x86_64_apple_darwin=o64-clang \
#     CXX_x86_64_apple_darwin=o64-clang++

# COPY ./Cargo.lock ./Cargo.lock
# COPY ./Cargo.toml ./Cargo.toml
# RUN cargo build --target $RUST_TARGET --release

# RUN rm -f target/${RUST_TARGET}/release/deps/silevri*
# RUN rm src/*.rs
# COPY . .
# RUN cargo build --target $RUST_TARGET --release
# RUN echo "successfully built an ${RUST_TARGET} executable"

# FROM scratch
FROM debian
ARG CRATE_NAME
WORKDIR /silevril
COPY --from=build-windows /${CRATE_NAME}/target/x86_64-pc-windows-gnu/release/${CRATE_NAME}.exe /binaries/windows-x86_64/${CRATE_NAME}.exe
# COPY --from=build-macos /${CRATE_NAME}/target/x86_64-apple-darwin/release/${CRATE_NAME} /binaries/darwin-x86_64/${CRATE_NAME}
COPY --from=build-linux /${CRATE_NAME}/target/x86_64-unknown-linux-musl/release/${CRATE_NAME} /binaries/linux-x86_64/${CRATE_NAME}
