# Design Doc For Silevril

# ( WORK IN PROGRESS) MOST OF THIS STUFF IS NOT / MAY NEVER BE IMPLEMENTED


## Sil-R (Silevril)

Sil-R is a 2D traditional roguelike set in the First Age of Middle Earth. The game is turn-based and centers on player's descent into 
Angband in order to wrest a silmaril from Morgoth's crown. The player fights through procedurally generated levels to
steal the silmaril, and must then fight their way back out of Angband to win the game.

# Characters

The player controls an elf. Unlike traditional games there are no classes, but the three races of elf available will
 work more like difficulty levels. The races will offer a descreasing amount of stat boosts to your characters
  starting stats.

# Story
Not a story heavy game, get the silmaril, get out, hope that the player's imagination does some heavy lifting until I figure out how to add tilesets.

# Theme

First Age of Middle Earth, with an attempt to keep it as theme appropriate as possible. While heavy development is
 underway there will be lorebreaking items in the game. Assume all the good guys are dead and have a cool item drop, and the bad guys are all still alive for you to fight.
 
# Story Progression
No benefits kept over from previous runs at all. Permadeath. You gain all benefits for a single run only. You can go
 both up and down in the dungeon, maybe returning to a town to sell goods. Progression on levels is preserved on the
  way down and up, however reinforcements may have arrived while the player was gone from that level.

1. Game start's in town/the players cabin. Player starts with some starting items.
2. Levels start out with a forest
3. Forest leads to a cave
4. Cave descends into a deeper cave with an orc encampment
5. Deep caverns transition to a dwarf fortress
.
.
.
.
100. Morgoths Throne Room
101. Fight your way back out of Angband

Currently I plan on implementing Town Portals for dev purposes but I will probably remove this functionality outside
 of as a cheat option.
 
 # Gameplay
 
 You start with little, subsist off what you find, kill or evade monsters that you encounter, and take their stuff
 . Item identification, magical items, stats, modifications for those stats, multiple ways to play and beat the game
  (you do not need to kill morgoth). Game will be difficult but tons of cheats / settings to swap to cater difficulty to anyones needs.
   
# Goals

## Skills Asked of the User
* Navigating different types of dungeons
* Tactical combat, learning AI behavior and keeping track of their terrain / inventory in order to increase their
 chances of surviving. 
 
* Item identification will involve a skill check each turn to passively learn what an item does
 , wielding the item will speed this process up.

* Stat management

* Long and short term resource management (always need to have a torch, etc.)

* Multiple Builds should be possible to pull off

# Game Mechanics

## Items and Buffs
Items:
* Wearable Equipment (armor, clothes, etc.)
* Wearable specials (amulets, rings, etc.)
* Melee weapons
* Ranged weapons
* Consumables (potions, scrolls, etc.)
* Minimum of junk as there will be nowhere to sell items
* Charged items?
* Food?
* Everything should have a weight attached to it
* Magic items should need to be identified 
* Forges for blacksmithing

## Progression
* No levels, but seeing/fighting enemies gives you experience points. These points can either be spent on skills or
 player stats.
 * Levels increase in difficulty as you descend into the dungeon
 
## Losing
Morgue file. Game over. 

# Art Style 
ASCII, hopefully tilesets one day

# Music and sound
NONE

# Technical Details
Written in Rust, using RLTK_RS for its back-end. It will support all the platforms on which Rust can compile and link
 to  OpenGL, including Web Assembly for browser-based play. 

## WASM Downsides
You cannot save json files for save files to webassembly.

All RAW files must be embedded in the rust executable or shipped with the binary unless I embed them. Annoying since it means the game will need to recompile whenever the data is changed. 

## Raw File Format

# Town Details
## Buildings
Temple
Bar
Blacksmith
Clothier
Alchemist


# Unsorted
# Everything Below Is Subject To Change

## Design Notes:

• The aim is to free a Silmaril from Morgoth’s crown and escape
• The game is much shorter than Angband and most of its predecessor
• Morgoth is found at 1,000 ft
• A winning game should take about 10 hours
• There is no town or word of recall
• Theme: The First Age of Middle Earth

•There are no classes, or experience levels
• Instead there are 8 skills to improve
• With a tree of special abilities for each

•There is no / much less explicit magic than its predecessors
• e.g. no fireballs, no teleportation... (Subject to change)

• There is an effective time limit
• Over time you can no longer find your way to the shallower depths
• You can thus play each level more than once, but not indefinitely


Light and darkness levels are very important for the player to be aware of

## System changes From Sil/Angband

### Stats
• Based around the average for the Men of the first age (set as 0)
• There are no ‘stat potions’, so initial stats matter a lot more
• Int/Wis/Cha are replaced with Grace

• Hit points don’t go up with experience
• They are solely based on your Constitution
• You die at 0 (instead of –1 in Angband)

### Elements
• There are only four elemental attacks (fire, cold, poison, dark)
• Resistances stack, reducing damage to 1/2, then 1/3,then 1/4, ...
• Dark resistance is determined by your light level
• Poison does no direct damage, but damages over time

•Speed is much less fine grained and less available
• The only speeds are 1, 2, 3, 4
• normal speed is 2, so +1 speed makes you move 50% faster

•You can only tunnel with shovels and mattocks
