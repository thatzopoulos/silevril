# Silevril (Sil-R)

![game-play](resources/gameplay.png?raw=true "gameplay")
Sil-R is a roguelike inspired by Tolkien, Angband, and its many variants. The game is written in Rust and uses the rltk_rs and specs crates.

The game is turn based and centers on the player's descent into Angband in order to wrest a shining silmaril from Morgoth's crown. The player fights through procedurally generated levels to steal the jewel and then must fight their way back out of Angband to win the game.

# Installation

## Normal Build

```sh
$ git clone https://gitlab.com/thatzopoulos/silevril
...
$ cd silevril
$ cargo run --release
```

## Wasm-Build
Silevril can also be compiled to wasm with the following commands:

```
cargo build --target wasm32-unknown-unknown --release
wasm-bindgen --no-modules ./target/wasm32-unknown-unknown/release/silevril.wasm --out-dir wasm-silevril/
```

You can play the wasm version of the game here : https://thatzopoulos.gitlab.io/silevril
The wasm build of Silevril is the same as the normal build of Silevril except
you cannot save your game currently.
