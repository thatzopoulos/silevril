# Changelog All notable changes to this project will be documented in this file.

## [0.4.0] 2019/11/26
### Added
- Added town to game
- Added changelog and started using [Semantic Versioning](https://semver.org/spec/v2.0.0.html)
### Changed 
- Swapped items/weapons/spawn tables/etc to json files
### Removed
- Temporarily removed WFC from being applied to maps

## [0.4.1] 2019/11/30
### 
- Added bystanders to town
- Added basic buildings to starting town in game
- Added dialogue option to npcs
- Changed font form 8x8 to 16x16

## [0.4.2] 2019/12/08
### Added
- New Hover Tooltips
- Map Names
- New UI
- RPG DiceRolls for combat
- Stats (Strength, Constitution, Dexterity, Intelligence)
### Changed
- NPCs have stats now
### Removed
- Shift + Number to select inventory items is currently broken

## [0.4.3] 2019/12/15
### Added
- Forest Level
- Loot Drops
- Carnivore and Herbivore system
### Changed
- New Monsters
- Players Starting Health and Health Per Level Increased

## [0.5.0] 2020/01/18
### Added
- XP points
### Changed
- Raised player starting health to make forest level playable
- replaced instances of `&s.to_string()` with just `&s` in gui.rs
- Fixed Bug where all items and enemies spawned 2 at a time in the same cell
- fixed screen size of wasm version
- touched up wallpaper REXpaint file
- death screen exits on Q and not on any key

## [0.5.1] 2020/01/31
### Added
- Player can now backtrack to previous levels via upwards staircases
- WASM now works with backtracking up staircases
### Changed
- Replaced all println! calls with console log calls from rltk for WASM
compatability
- Swapped initial window creation in main to new RltkBuilder pattern

## [0.5.2]
### Added
- Caverns with glowing gelationous slime, giant spiders, and bats
- Added light sources
- Added cheats menu
### Changed 
- Once you leave the forest you now enter caverns
- Bandits no longer appear outside of the forest
- Kobolds were moved to lower levels
- Health Potions are not murky brown potions

## [0.6.0] 2020/04/21
### Added
- Factions
### Changed
- Reworked all AI systems
- Implemented Generic Game Ticks

## [0.6.1] 2020/06/21
### Added
- Vendors that players can buy items from and sell items to
- Items now all have weights and gold values
- Initiative System
- Gold has been added
### Changed
- Certain enemies now drop varying amounts of gold

## [0.7.0] 2020/07/01 
### Added 
- Added more cheats: Heal, GodMode, Reveal Map
- Added new level type, deep caverns, with an orc encampment and miniboss
- Props can be light sources
### Changed
- Message logs now appear in reverse order
- Added new equipment

## [0.8.0] 2020/10/17
### Added
- Added town portals

## [0.9.0] 2021/03/25
### Added
- Added item identification system for potions and scrolls with unique names per run
- Added Magic Weapon, currently denoted with a +1 at the end of the name
- Added Cursed Items/Weapons/Armor with a -1 at the end of the name
- Added Magic Items
- Added Message Passing System to get around Specs limitation on passing around tons of data storages at once
- Added scrolls of remove curse and identification
### Changed
- Weapons and armor can now drop as special Magic or cursed versions of themselves
- Scrolls and potions must now be identified by activating, magic weapons and armor must be identified by wielding
- Reworked inventory, trigger, and effects systems
### Bugfixes
- Found bug where fireball scroll is not destroyed after use if it misses / does no damage to anything