use super::{
    ApplyMove, ApplyTeleport, BlocksTile, EntityMoved, Map, OtherLevelPosition, Position, RunState,
    Viewshed,
};
use specs::prelude::*;

pub struct MovementSystem {}

impl<'a> System<'a> for MovementSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteExpect<'a, Map>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, BlocksTile>,
        Entities<'a>,
        WriteStorage<'a, ApplyMove>,
        WriteStorage<'a, ApplyTeleport>,
        WriteStorage<'a, OtherLevelPosition>,
        WriteStorage<'a, EntityMoved>,
        WriteStorage<'a, Viewshed>,
        ReadExpect<'a, Entity>,
        WriteExpect<'a, RunState>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            map,
            mut position,
            _blockers,
            entities,
            mut apply_move,
            mut apply_teleport,
            mut other_level,
            mut moved,
            mut viewsheds,
            player_entity,
            mut runstate,
        ) = data;

        // Iterate all entries that are marked as teleporting
        // Apply teleports
        for (entity, teleport) in (&entities, &apply_teleport).join() {
            if teleport.dest_depth == map.depth {
                // if on the current depth, add an apply_move component to indicate that we are
                // moving across the map
                apply_move
                    .insert(
                        entity,
                        ApplyMove { dest_idx: map.xy_idx(teleport.dest_x, teleport.dest_y) },
                    )
                    .expect("Unable to insert");
            } else if entity == *player_entity {
                // It's the player - we have a mess
                *runstate = RunState::TeleportingToOtherLevel {
                    x: teleport.dest_x,
                    y: teleport.dest_y,
                    depth: teleport.dest_depth,
                };
            } else if let Some(pos) = position.get(entity) {
                // if not the player, remove their POsition component and add an
                // OtherLevelPosition component to move the entity to the teleport destination
                let idx = map.xy_idx(pos.x, pos.y);
                let dest_idx = map.xy_idx(teleport.dest_x, teleport.dest_y);
                crate::spatial::move_entity(entity, idx, dest_idx);
                other_level
                    .insert(
                        entity,
                        OtherLevelPosition {
                            x: teleport.dest_x,
                            y: teleport.dest_y,
                            depth: teleport.dest_depth,
                        },
                    )
                    .expect("Unable to insert");
                position.remove(entity);
            }
        }
        apply_teleport.clear(); // remove all teleport intentions since they have been processed

        // Apply broad movement
        for (entity, movement, mut pos) in (&entities, &apply_move, &mut position).join() {
            let start_idx = map.xy_idx(pos.x, pos.y); // start index
            let dest_idx = movement.dest_idx as usize; // destination index
            crate::spatial::move_entity(entity, start_idx, dest_idx);
            // if entity blocks the tile, clear the blocking in the source tile and set the
            // blocking status in the destination tile?
            pos.x = movement.dest_idx as i32 % map.width;
            pos.y = movement.dest_idx as i32 / map.width;
            if let Some(vs) = viewsheds.get_mut(entity) {
                vs.dirty = true; // mark viewshed as dirty
            }
            moved.insert(entity, EntityMoved {}).expect("Unable to insert"); // apply an EntityMoved component
        }
        apply_move.clear();
    }
}
