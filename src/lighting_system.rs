//! # Lighting System
//!
//! If the map is outdoors, simply returns without doing anything.
//!
//! Otherwise:
//! Sets the Entire Map's lighting to be dark and then
//! iterates all entitites that have a position, viewshed, and light source.
//!
//! For each of these entitites, it iterates all visible tiles and then
//! calculates the distance to the light source for the visiblie tile, and
//! inverts it. (Further from the light source is darker)
//!
//!  This is divided by the
//! light's range, to scale it into the 0.1 range. This lighting amount is added
//! to the tile's lighting
use super::{LightSource, Map, Position, Viewshed};
use rltk::RGB;

use specs::prelude::*;

pub struct LightingSystem {}

impl<'a> System<'a> for LightingSystem {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteExpect<'a, Map>,
        ReadStorage<'a, Viewshed>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, LightSource>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut map, viewshed, positions, lighting) = data;

        if map.outdoors {
            return;
        }

        let black = RGB::from_f32(0.0, 0.0, 0.0);
        for l in map.light.iter_mut() {
            *l = black;
        }

        for (viewshed, pos, light) in (&viewshed, &positions, &lighting).join() {
            let light_point = rltk::Point::new(pos.x, pos.y);
            let range_f = light.range as f32;
            for t in viewshed.visible_tiles.iter() {
                if t.x > 0 && t.x < map.width && t.y > 0 && t.y < map.height {
                    let idx = map.xy_idx(t.x, t.y);
                    let distance = rltk::DistanceAlg::Pythagoras.distance2d(light_point, *t);
                    let intensity = (range_f - distance) / range_f;

                    map.light[idx] = map.light[idx] + (light.color * intensity);
                }
            }
        }
    }
}
