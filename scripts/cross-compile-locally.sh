#!/bin/sh

# echo Building silevril binaries with cross
# cross build --release --target x86_64-pc-windows-gnu
# cross build --release --target x86_64-unknown-linux-musl

echo Building thatzopoulos/sil-cross with Docker
docker build -t thatzopoulos/sil-cross .
docker container create --name extract thatzopoulos/sil-cross 
docker container cp extract:/binaries/ ./binaries
docker container rm -f extract